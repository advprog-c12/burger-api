package com.burgerapi.burger.API;

import com.burgerapi.burger.API.bun.BreadProducer;
import com.burgerapi.burger.API.filling.FillingDecorator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BurgerApiApplicationTests {

    private Burger plain;
    private Burger ciabatta;
    private Burger potato;
    private Burger englishMuffin;


    @Test
    public void testThickBunBurgerSpecialCost() {
        //plain Bun Burger with mozarella, cheddar, hot sauce, onions, and pickels
        plain = BreadProducer.PLAIN.createBunToBeFilled();
        assertEquals(9000.0, plain.cost(), 0.001);

        plain = FillingDecorator.MOZARELLA_CHEESE.addFillingToBun(plain);
        assertEquals(13000.0, plain.cost(), 0.001);

        plain = FillingDecorator.CHEDDAR_CHEESE.addFillingToBun(plain);
        assertEquals(16000.0, plain.cost(), 0.001);

        plain = FillingDecorator.HOT_SAUCE.addFillingToBun(plain);
        assertEquals(17000.0, plain.cost(), 0.001);

        plain = FillingDecorator.ONIONS.addFillingToBun(plain);
        assertEquals(17500.0, plain.cost(), 0.001);

        plain = FillingDecorator.PICKELS.addFillingToBun(plain);
        assertEquals(18000.0, plain.cost(), 0.001);
    }
}


