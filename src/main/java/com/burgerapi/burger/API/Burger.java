package com.burgerapi.burger.API;

public abstract class Burger {

    protected String description = "Unidentified Burger";

    public String getDescription() {
        return description;
    }

    public abstract int cost();
}
