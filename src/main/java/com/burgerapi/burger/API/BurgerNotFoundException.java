package com.burgerapi.burger.API;

public class BurgerNotFoundException extends RuntimeException{
    public BurgerNotFoundException(Long id) {
        super("Could not find burger " + id);
    }
}
