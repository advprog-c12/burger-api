package com.burgerapi.burger.API;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class BurgerApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(BurgerApiApplication.class, args);
    }
}
