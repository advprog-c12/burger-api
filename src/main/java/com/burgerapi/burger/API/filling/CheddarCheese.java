package com.burgerapi.burger.API.filling;

import com.burgerapi.burger.API.Burger;


public class CheddarCheese extends Filling {
    public CheddarCheese(Burger burger) {
        this.burger = burger;
    }

    @Override
    public String getDescription() {
        return burger.getDescription() + ", Cheddar Cheese";
    }

    @Override
    public int cost() {
        return burger.cost() + 3000;
    }
}
