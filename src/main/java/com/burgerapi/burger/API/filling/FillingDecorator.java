package com.burgerapi.burger.API.filling;

import com.burgerapi.burger.API.Burger;

public enum FillingDecorator {

    HOT_SAUCE,
    ONIONS,
    PICKELS,
    SCRAMBLED_EGG,
    SMOKED_BEEF,
    MOZARELLA_CHEESE,
    CHEDDAR_CHEESE;

    /**
     * This function will decorate Burger Object with a Filling Object.
     *
     * @param bun that want to be filled
     * @return bun that had been filled
     */
    public Burger addFillingToBun(Burger bun) {
        switch (this) {
            case HOT_SAUCE:
                bun = new HotSauce(bun);
                break;
            case ONIONS:
                bun = new Onions(bun);
                break;
            case PICKELS:
                bun = new Pickels(bun);
                break;
            case SCRAMBLED_EGG:
                bun = new ScrambledEgg(bun);
                break;
            case SMOKED_BEEF:
                bun = new SmokedBeef(bun);
                break;
            case MOZARELLA_CHEESE:
                bun = new MozarellaCheese(bun);
                break;
            case CHEDDAR_CHEESE:
                bun = new CheddarCheese(bun);
                break;
        }
        return bun;
    }
}
