package com.burgerapi.burger.API.filling;

import com.burgerapi.burger.API.Burger;


public class SmokedBeef extends Filling {

    public SmokedBeef(Burger burger) {
        this.burger = burger;
    }

    @Override
    public String getDescription() {
        return burger.getDescription() + ", Smoked Beef";
    }

    @Override
    public int cost() {
        return burger.cost() + 3000;
    }
}
