package com.burgerapi.burger.API.filling;

import com.burgerapi.burger.API.Burger;


public class Onions extends Filling {

    public Onions(Burger burger) {
        this.burger = burger;
    }

    @Override
    public String getDescription() {
        return burger.getDescription() + ", Onions";
    }

    @Override
    public int cost() {
        return burger.cost() + 500;
    }
}
