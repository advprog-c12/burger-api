package com.burgerapi.burger.API.filling;

import com.burgerapi.burger.API.Burger;


public class Pickels extends Filling {

    public Pickels(Burger burger) {
        this.burger = burger;
    }

    @Override
    public String getDescription() {
        return burger.getDescription() + ", Pickels";
    }

    @Override
    public int cost() {
        return burger.cost() + 500;
    }
}
