package com.burgerapi.burger.API.filling;

import com.burgerapi.burger.API.Burger;


public class HotSauce extends Filling {

    public HotSauce(Burger burger) {
        this.burger = burger;
    }

    @Override
    public String getDescription() {
        return burger.getDescription() + ", Hot Sauce";
    }

    @Override
    public int cost() {
        return burger.cost() + 1000;
    }
}
