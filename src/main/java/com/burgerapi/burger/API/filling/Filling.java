package com.burgerapi.burger.API.filling;

import com.burgerapi.burger.API.Burger;


public abstract class Filling extends Burger {
    protected Burger burger;
    public abstract String getDescription();
}
