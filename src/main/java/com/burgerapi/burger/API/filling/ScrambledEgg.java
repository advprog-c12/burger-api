package com.burgerapi.burger.API.filling;

import com.burgerapi.burger.API.Burger;


public class ScrambledEgg extends Filling {

    public ScrambledEgg(Burger burger) {
        this.burger = burger;
    }

    @Override
    public String getDescription() {
        return burger.getDescription() + ", Scrambled Egg";
    }

    @Override
    public int cost() {
        return burger.cost() + 4000;
    }
}
