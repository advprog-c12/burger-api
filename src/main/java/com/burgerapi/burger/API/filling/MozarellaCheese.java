package com.burgerapi.burger.API.filling;

import com.burgerapi.burger.API.Burger;

public class MozarellaCheese extends Filling {
    public MozarellaCheese(Burger burger) {
        this.burger = burger;
    }

    @Override
    public String getDescription() {
        return burger.getDescription() + ", Mozarella Cheese";
    }

    @Override
    public int cost() {
        return burger.cost() + 4000;
    }
}
