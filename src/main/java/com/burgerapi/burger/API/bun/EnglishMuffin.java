package com.burgerapi.burger.API.bun;

import com.burgerapi.burger.API.Burger;

public class EnglishMuffin extends Burger {

    public EnglishMuffin() {
        description = "English Muffin Bun";
    }

    @Override
    public int cost() {
        return 12000;
    }
}
