package com.burgerapi.burger.API.bun;

import com.burgerapi.burger.API.Burger;

public class Plain extends Burger {
    public Plain() {
        description = "Plain Bun";
    }

    @Override
    public int cost() {
        return 9000;
    }
}
