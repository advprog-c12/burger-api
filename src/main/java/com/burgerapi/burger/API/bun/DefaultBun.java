package com.burgerapi.burger.API.bun;

import com.burgerapi.burger.API.Burger;

public class DefaultBun extends Burger {
    public DefaultBun() {
        description = "";
    }

    @Override
    public int cost() {
        return 0;
    }
}
