package com.burgerapi.burger.API.bun;

import com.burgerapi.burger.API.Burger;


public enum BreadProducer {
    ENGLISH_MUFFIN,
    PLAIN,
    CIABATTA,
    POTATO;

    /**
     * This method will create a Food object that represent a bun that want to be created.
     *
     * @return Food that represent bun type, Default bun is plain
     */
    public Burger createBunToBeFilled() {
        Burger bun;
        switch (this) {
            case ENGLISH_MUFFIN:
                bun = new EnglishMuffin();
                break;
            case PLAIN:
                bun = new Plain();
                break;
            case CIABATTA:
                bun = new Ciabatta();
                break;
            case POTATO:
                bun = new Potato();
                break;
            default:
                bun = new DefaultBun();
                break;
        }
        return bun;
    }
}
