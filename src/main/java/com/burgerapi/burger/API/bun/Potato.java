package com.burgerapi.burger.API.bun;

import com.burgerapi.burger.API.Burger;

public class Potato extends Burger {
    public Potato() {
        description = "Potato Bun";
    }

    @Override
    public int cost() {
        return 10000;
    }
}
