package com.burgerapi.burger.API.bun;

import com.burgerapi.burger.API.Burger;

public class Ciabatta extends Burger {
    public Ciabatta() {
        description = "Ciabatta Bun";
    }

    @Override
    public int cost() {
        return 11000;
    }
}
