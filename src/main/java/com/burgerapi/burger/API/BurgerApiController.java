package com.burgerapi.burger.API;

import com.burgerapi.burger.API.bun.*;
import com.burgerapi.burger.API.filling.*;
import com.burgerapi.burger.API.model.Bun;
import com.burgerapi.burger.API.model.Filling;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class BurgerApiController {
    private static Map<String, Bun> bunRepo = new HashMap<>();

    static {
        int counter = 0;
        Bun plain = new Bun();
        Burger plainBun = BreadProducer.PLAIN.createBunToBeFilled();
        plain.setId(String.valueOf(counter++));
        plain.setName("Plain bun");
        plain.setDescription(plainBun.getDescription());
        plain.setCost(plainBun.cost());
        plain.setImageLink("https://d2gk7xgygi98cy.cloudfront.net/31-3-large.jpg");
        bunRepo.put(plain.getId(), plain);

        Bun potato = new Bun();
        Burger potatoBun = BreadProducer.POTATO.createBunToBeFilled();
        potato.setId(String.valueOf(counter++));
        potato.setName("Potato bun");
        potato.setDescription(potatoBun.getDescription());
        potato.setCost(potatoBun.cost());
        potato.setImageLink("https://www.handletheheat.com/wp-content/uploads/2016/02/Potato-Buns-Square-550x550.jpg");
        bunRepo.put(potato.getId(), potato);

        Bun englishMuffin = new Bun();
        Burger englishMuffinBun = BreadProducer.ENGLISH_MUFFIN.createBunToBeFilled();
        englishMuffin.setId(String.valueOf(counter++));
        englishMuffin.setName("English Muffin bun");
        englishMuffin.setDescription(englishMuffinBun.getDescription());
        englishMuffin.setCost(englishMuffinBun.cost());
        englishMuffin.setImageLink("https://ecs7.tokopedia.net/img/cache/700/product-1/2019/1/22/10825996/10825996_03a9947c-8714-48bf-b699-3fcee9d1182a_2048_1536.jpg");
        bunRepo.put(englishMuffin.getId(), englishMuffin);

        Bun ciabatta = new Bun();
        Burger ciabattaBun = BreadProducer.CIABATTA.createBunToBeFilled();
        ciabatta.setId(String.valueOf(counter++));
        ciabatta.setName("Ciabatta bun");
        ciabatta.setDescription(ciabattaBun.getDescription());
        ciabatta.setCost(ciabattaBun.cost());
        ciabatta.setImageLink("https://www.tasteofhome.com/wp-content/uploads/2017/10/Chipotle-Roast-Beef-Sandwiches_exps47889_SD1785603D31A_RMS-696x696.jpg");
        bunRepo.put(ciabatta.getId(), ciabatta);


    }

    private static Map<String, Filling> fillingRepo = new HashMap<>();

    static {
        int counter = 0;

        Filling cheddar = new Filling();
        CheddarCheese cheddarFilling = new CheddarCheese(new DefaultBun());
        cheddar.setId(String.valueOf(counter++));
        cheddar.setName("Cheddar Cheese");
        cheddar.setDescription(cheddarFilling.getDescription());
        cheddar.setCost(cheddarFilling.cost());
        fillingRepo.put(cheddar.getId(), cheddar);

        Filling hotSauce = new Filling();
        HotSauce hotSauceFilling = new HotSauce(new DefaultBun());
        hotSauce.setId(String.valueOf(counter++));
        hotSauce.setName("Hot Sauce");
        hotSauce.setDescription(hotSauceFilling.getDescription());
        hotSauce.setCost(hotSauceFilling.cost());
        fillingRepo.put(hotSauce.getId(), hotSauce);

        Filling mozarella = new Filling();
        MozarellaCheese mozarellaFilling = new MozarellaCheese(new DefaultBun());
        mozarella.setId(String.valueOf(counter++));
        mozarella.setName("Mozarella Cheese");
        mozarella.setDescription(mozarellaFilling.getDescription());
        mozarella.setCost(mozarellaFilling.cost());
        fillingRepo.put(mozarella.getId(), mozarella);

        Filling onions = new Filling();
        Onions onionsFilling = new Onions(new DefaultBun());
        onions.setId(String.valueOf(counter++));
        onions.setName("Onions");
        onions.setDescription(onionsFilling.getDescription());
        onions.setCost(onionsFilling.cost());
        fillingRepo.put(onions.getId(), onions);

        Filling pickels = new Filling();
        Pickels pickelsFilling = new Pickels(new DefaultBun());
        pickels.setId(String.valueOf(counter++));
        pickels.setName("Pickels");
        pickels.setDescription(pickelsFilling.getDescription());
        pickels.setCost(pickelsFilling.cost());
        fillingRepo.put(pickels.getId(), pickels);

        Filling egg = new Filling();
        ScrambledEgg eggFilling = new ScrambledEgg(new DefaultBun());
        egg.setId(String.valueOf(counter++));
        egg.setName("Scrambled Egg");
        egg.setDescription(eggFilling.getDescription());
        egg.setCost(eggFilling.cost());
        fillingRepo.put(egg.getId(), egg);

        Filling beef = new Filling();
        SmokedBeef beefFilling = new SmokedBeef(new DefaultBun());
        beef.setId(String.valueOf(counter++));
        beef.setName("Smoked Beef");
        beef.setDescription(beefFilling.getDescription());
        beef.setCost(beefFilling.cost());
        fillingRepo.put(beef.getId(), beef);
    }

    private static Map<String, Filling> decoratedRepo = new HashMap<>();

    static {
        int counter = 0;

        Filling filling1 = new Filling();
        Burger burger1 = new MozarellaCheese(new EnglishMuffin());
        filling1.setId(String.valueOf(counter++));
        filling1.setName("Paket 1");
        filling1.setDescription(burger1.getDescription());
        filling1.setCost(burger1.cost());
        decoratedRepo.put(filling1.getId(), filling1);

        Filling filling2 = new Filling();
        Burger burger2 = new Pickels(new Onions(new Potato()));
        filling2.setId(String.valueOf(counter++));
        filling2.setName("Paket 2");
        filling2.setDescription(burger2.getDescription());
        filling2.setCost(burger2.cost());
        decoratedRepo.put(filling2.getId(), filling2);

        Filling filling3 = new Filling();
        Burger burger3 = new SmokedBeef(new HotSauce(new Plain()));
        filling3.setId(String.valueOf(counter++));
        filling3.setName("Paket 3");
        filling3.setDescription(burger3.getDescription());
        filling3.setCost(burger3.cost());
        decoratedRepo.put(filling3.getId(), filling3);

        Filling filling4 = new Filling();
        Burger burger4 = new ScrambledEgg(new Onions(new Ciabatta()));
        filling4.setId(String.valueOf(counter++));
        filling4.setName("Paket 4");
        filling4.setDescription(burger4.getDescription());
        filling4.setCost(burger4.cost());
        decoratedRepo.put(filling4.getId(), filling4);
    }

    @GetMapping("/")
    public String index() {
        return "Api";
    }

    @RequestMapping(value = "buns")
    public ResponseEntity<Object> getBun() {
        return new ResponseEntity<>(bunRepo.values(), HttpStatus.OK);
    }

    @RequestMapping(value = "fillings")
    public ResponseEntity<Object> getFilling() {
        return new ResponseEntity<>(fillingRepo.values(), HttpStatus.OK);
    }

    @RequestMapping(value = "decorated")
    public ResponseEntity<Object> getDecorated() {
        return new ResponseEntity<>(decoratedRepo.values(), HttpStatus.OK);
    }

}
