**This project gives the burger-queen the burger object service**

[![coverage report](https://gitlab.com/advprog-c12/burger-api/badges/master/coverage.svg)](https://gitlab.com/advprog-c12/burger-api/commits/master)

[![pipeline status](https://gitlab.com/advprog-c12/burger-api/badges/master/pipeline.svg)](https://gitlab.com/advprog-c12/burger-api/commits/master)

made by c12 with love <3

link: http://burger-queen-api.herokuapp.com/